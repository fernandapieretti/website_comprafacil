var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('minifyCSS', function() {
    return gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/jquery-ui-dist/jquery-ui.min.css',
        'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
        'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
        'assets/css/*.sass'
    ])
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('assets/css')); 
});

gulp.task('minifyJS', function() {
    return gulp.src([        
        'node_modules/jquery/dist/jquery.js',
        'node_modules/jquery-ui-dist/jquery-ui.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/owl.carousel/dist/owl.carousel.min.js',
        'assets/js/script.js'
    ])
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('assets/js'));
});

gulp.task('minifyCSS:watch', function () {
    gulp.watch('assets/css/*.sass', ['minifyCSS']);
});

gulp.task('default', ['minifyCSS', 'minifyJS', 'minifyCSS:watch']);