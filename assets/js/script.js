$(window).on('load', function() {
    $(".owl-carousel").owlCarousel({
        nav: true,
        dots: false,
        items: 4,
        responsive:{
            0: {
                items:1
            },
            576: {
                items: 2
            },
            768: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 200,
        values: [ 10, 110 ],
        slide: function( event, ui ) {
            $( "#amount1" ).val( "R$ " + ui.values[ 0 ] );
            $( "#amount2" ).val( "R$ " + ui.values[ 1 ] );
        }
    });
    
    $( "#amount1" ).val( "R$ " + $( "#slider-range" ).slider( "values", 0 ) );
    $( "#amount2" ).val( "R$ " + $( "#slider-range" ).slider( "values", 1 ) );

    $( "#tabs" ).tabs();

    $("header .carrinho").click(function() {
        $(".meu-carrinho").addClass("active");
        $(".mask").fadeIn();
    });
    $(".meu-carrinho .fechar").click(function() {
        $(".meu-carrinho").removeClass("active");
        $(".mask").fadeOut();
    });

    $( "#accordion" ).accordion({
        heightStyle: "content"
      });
});